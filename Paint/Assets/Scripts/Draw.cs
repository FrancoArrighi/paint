using System.Collections.Generic;
using UnityEngine;

public class Draw : MonoBehaviour
{
    [SerializeField] private Camera _camera;
    [SerializeField] private LineRenderer linePrefab = null;
    private LineRenderer currentLine;
    private List<Vector3> points = new List<Vector3>();

    [SerializeField] private Material currentMat;
    [SerializeField] private Color currentColor;
    [SerializeField] private float currentWidth;

    [SerializeField] private Texture2D _texture;

    public Transform sphereThickness;
    public float speed = 0.1f;
    public float movementRange = 0.45f; // El rango en el que se mover� la pelota

    public float minWidth = 0.02f;
    public float maxWidth = 0.3f;
    public float maxPosSphere = 0.45f;
    public float minPosSphere = -0.45f;

    public Stack<LineRenderer> lines = new Stack<LineRenderer>();

    void Start()
    {
        _camera = Camera.main;
    }

    void Update()
    {
        if (!GameManager.instance.playerPainting || GameManager.instance.savingPicture)
        {
            return;
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            DeleteLines();
        }
        if (Input.GetKeyDown(KeyCode.Z))
        {
            DeleteLastLine();
        }
        MoveSphere();
        SelectColor();
        SetLineWidth();

        if (HittingFrame())
        {
            if (Input.GetMouseButtonDown(0))
            {
                CreateLine();
            }
            if (Input.GetMouseButton(0))
            {
                AddPoint();
            }
        }
    }
    private void CreateLine()
    {
        // Cada vez que creo una llevo las demas un cachito para atras nomas
        currentLine = Instantiate(linePrefab);
        ChangeColor(currentColor);
        ChangeWidth();
        lines.Push(currentLine);
        currentLine.transform.SetParent(transform, true);
        points.Clear();
    }
    private void UpdateLinePoints()
    {
        if (currentLine != null && points.Count > 1)
        {
            currentLine.positionCount = points.Count;
            currentLine.SetPositions(points.ToArray());
        }
    }
    private void AddPoint()
    {
        var Ray = _camera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(Ray, out hit))
        {
            if (hit.collider.CompareTag("writeable"))
            {
                points.Add(hit.point);
                UpdateLinePoints();
                return;
            }
        }
        else { return; }
    }
    public bool HittingFrame()
    {
        var Ray = _camera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(Ray, out hit))
        {
            if (hit.collider.CompareTag("writeable"))
            {
                return true;
            }
            else { return false; }
        }
        else { return false; }
    }
    private void SelectColor()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;

            if (Physics.Raycast(_camera.ScreenPointToRay(Input.mousePosition), out hit))
            {
                if (hit.collider.CompareTag("palette"))
                {
                    Vector2 pixelUV = hit.textureCoord;
                    pixelUV.x *= _texture.width;
                    pixelUV.y *= _texture.height;

                    Color color = _texture.GetPixel(Mathf.FloorToInt(pixelUV.x), Mathf.FloorToInt(pixelUV.y));
                    currentColor = color;
                    Debug.Log("Selected Color: " + currentColor);
                }
                if (hit.collider.CompareTag("brushes"))
                {
                    currentMat = hit.collider.GetComponent<MeshRenderer>().material;
                }
            }
        }
    }
    private void ChangeColor(Color newColor)
    {
        Gradient gradient = new Gradient();

        GradientColorKey[] colors = new GradientColorKey[2];
        colors[0] = new GradientColorKey(newColor, 0);
        colors[1] = new GradientColorKey(newColor, 1);

        gradient.colorKeys = colors;
        currentLine.colorGradient = gradient;

        currentLine.material = currentMat;
    }

    private void MoveSphere()
    {
        float scroll = Input.mouseScrollDelta.y;

        float newPos = sphereThickness.localPosition.y + scroll * speed * Time.deltaTime;

        newPos = Mathf.Clamp(newPos, -movementRange, movementRange);

        sphereThickness.localPosition = new Vector3(sphereThickness.localPosition.x, newPos, sphereThickness.localPosition.z);
    }
    private void SetLineWidth()
    {
        float t = (sphereThickness.localPosition.y - minPosSphere) / (maxPosSphere - minPosSphere);

        currentWidth = Mathf.Lerp(minWidth, maxWidth, t);
    }

    private void ChangeWidth()
    {
        currentLine.startWidth = currentWidth;
        currentLine.endWidth = currentWidth;
    }
    private void DeleteLines()
    {
        if (lines != null)
        {
            foreach (LineRenderer line in lines)
            {
                Destroy(line.gameObject);
            }
            lines.Clear();
        }
    }
    private void DeleteLastLine()
    {
        if (lines.Count > 0)
        {
            LineRenderer line = lines.Peek();
            lines.Pop();
            Destroy(line.gameObject);
        }
    }
}
