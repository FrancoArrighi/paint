using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using TMPro;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

public class SaveSystem : MonoBehaviour
{
    PicturesInfoSerializer pictureInfoSerializer;

    public GameObject SaveInterface;
    public Image Img_Picture;
    public TMP_Dropdown posInGallery;
    public TMP_Dropdown mat_frameIndex;
    public TMP_InputField txt_plaque;

    public Camera captureCamera;
    public GameObject targetObject;
    public int captureWidth = 512;
    public int captureHeight = 512;

    string folderPath_screenshots = Path.Combine(Application.dataPath, "Screenshots");
    public static int screenshotCount = 0;

    private RenderTexture rt;
    private Texture2D screenShot;

    public static SaveSystem instance;
    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        pictureInfoSerializer = FindObjectOfType<PicturesInfoSerializer>();
        captureCamera.enabled = false;
        GetScreenshotCount(folderPath_screenshots);
        GalleryManager.instance.SetInitialPictures(GiveScreenshots());
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.G) && GameManager.instance.playerPainting && !GameManager.instance.savingPicture)
        {
            OpenSaveInterface();
        }
    }

    public Texture2D TakeScreenshot()
    {
        rt = new RenderTexture(captureWidth, captureHeight, 24);
        captureCamera.targetTexture = rt;

        captureCamera.enabled = true;
        captureCamera.Render();
        captureCamera.enabled = false;

        RenderTexture.active = rt;
        screenShot = new Texture2D(captureWidth, captureHeight, TextureFormat.RGB24, false);
        screenShot.ReadPixels(new Rect(0, 0, captureWidth, captureHeight), 0, 0);
        screenShot.Apply();
        return screenShot;
    }

    public void OpenSaveInterface()
    {
        GameManager.instance.savingPicture = true;
        txt_plaque.text = "";
        SaveInterface.SetActive(true);
        Texture2D texture = TakeScreenshot();
        Sprite newSprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
        Img_Picture.sprite = newSprite;
    }

    public void SavePicture() // cuando apreta el boton de guardar
    {
        SavePictureInFolder();
        SavePictureInGallery();
        pictureInfoSerializer.SavePictureInfo(txt_plaque.text, int.Parse(posInGallery.options[posInGallery.value].text), screenshotCount, true, int.Parse(mat_frameIndex.options[mat_frameIndex.value].text));
        Debug.Log("pos: " + int.Parse(posInGallery.options[posInGallery.value].text));
        SaveInterface.SetActive(false);
        GameManager.instance.savingPicture = false;
    }

    public void SavePictureInGallery()
    {
        GalleryManager.instance.SetPictureInGallery(screenShot, txt_plaque.text, int.Parse(posInGallery.options[posInGallery.value].text), int.Parse(mat_frameIndex.options[mat_frameIndex.value].text));

        captureCamera.targetTexture = null;
        RenderTexture.active = null;
        Destroy(rt);
    }

    public void SavePictureInFolder()
    {
        byte[] bytes = screenShot.EncodeToPNG();

        if (!Directory.Exists(folderPath_screenshots))
        {
            Directory.CreateDirectory(folderPath_screenshots);
        }

        string filename = string.Format("Screenshot_{0}_{1}.png", screenshotCount, System.DateTime.Now.ToString("yyyy-MM-dd"));
        string filePath = Path.Combine(folderPath_screenshots, filename);
        try
        {
            File.WriteAllBytes(filePath, bytes);
            Debug.Log($"Screenshot saved to {filePath}");
            screenshotCount++; // Incrementa el contador despu�s de guardar la captura
        }
        catch (IOException e)
        {
            Debug.LogError($"Failed to save screenshot: {e.Message}");
        }
    }

    private void GetScreenshotCount(string folderPath)
    {
        string[] files = Directory.GetFiles(folderPath, "Screenshot_*.png");
        screenshotCount = files.Length;
    }

    public List<byte[]> GiveScreenshots()
    {
        string[] files = Directory.GetFiles(folderPath_screenshots, "Screenshot_*.png");

        Array.Sort(files, (x, y) => File.GetLastWriteTime(y).CompareTo(File.GetLastWriteTime(x)));

        int count = Mathf.Min(files.Length, 10);
        List<byte[]> screenshots = new List<byte[]>(count);

        for (int i = 0; i < count; i++)
        {
            byte[] fileData = File.ReadAllBytes(files[i]);
            screenshots.Add(fileData);
        }

        return screenshots;
    }
}
