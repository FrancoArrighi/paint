using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using static PicturesInfoSerializer;

public class PicturesInfoSerializer : MonoBehaviour
{
    public List<PictureInfo> pictures = new List<PictureInfo>();

    public void SavePictureInfo(string newText, int posInGallery, int posInFolder, bool inGallery, int frameType)
    {
        string path = Application.dataPath + "/picturesInfo.json";

        if (File.Exists(path))
        {
            ReadFile(path);
        }
        else
        {
            pictures = new List<PictureInfo>();
        }

        pictures.Where(p => p.posInGallery == posInGallery).ToList().ForEach(p => p.inGallery = false); // pongo en false la anterior imagen que estaba en la galeria 

        PictureInfo pictureInfo = new PictureInfo();

        pictureInfo.txt_plaque = newText;
        pictureInfo.posInGallery = posInGallery;
        pictureInfo.posInFolder = posInFolder;
        pictureInfo.inGallery = inGallery;
        pictureInfo.frameType = frameType;

        pictures.Add(pictureInfo);

        PicturesInfo picturesInfo = new PicturesInfo();
        picturesInfo.picturesInfo = pictures;

        string newJson = JsonUtility.ToJson(picturesInfo, true);
        File.WriteAllText(path, newJson);
    }

    public void LoadPicturesInfoFromFile()
    {
        string path = Application.dataPath + "/picturesInfo.json";

        if (File.Exists(path))
        {
            ReadFile(path);
        }
        else
        {
            Debug.LogWarning("No se encontr� el archivo JSON en " + path);
        }
    }

    private void ReadFile(string path)
    {
        string json = File.ReadAllText(path);
        PicturesInfo picturesInfo = JsonUtility.FromJson<PicturesInfo>(json);
        pictures = picturesInfo.picturesInfo;
    }

    [System.Serializable]
    public class PicturesInfo
    {
        public List<PictureInfo> picturesInfo;
    }
}
