using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PictureInfo
{
    public string txt_plaque = "";
    public int posInGallery = 0;
    public int posInFolder;
    public int frameType = 0;
    public bool inGallery = false;
}
