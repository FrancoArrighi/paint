using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamMovements : MonoBehaviour
{
    [SerializeField] Transform currentView;
    [SerializeField] Transform paintView; // 0 painting 1 player
    public float camSpeed;

    private void Start()
    {
        currentView = transform;
    }

    private void Update()
    {
        currentView = paintView;
    }
    private void LateUpdate()
    {
        transform.position = Vector3.Lerp(transform.position, currentView.position, Time.deltaTime * camSpeed);
        Vector3 currentAngle = new Vector3(
            Mathf.Lerp(transform.rotation.eulerAngles.x, currentView.rotation.eulerAngles.x, Time.deltaTime * camSpeed),
            Mathf.Lerp(transform.rotation.eulerAngles.y, currentView.rotation.eulerAngles.y, Time.deltaTime * camSpeed),
            Mathf.Lerp(transform.rotation.eulerAngles.z, currentView.rotation.eulerAngles.z, Time.deltaTime * camSpeed)
            );
        transform.eulerAngles = currentAngle;
    }
}
