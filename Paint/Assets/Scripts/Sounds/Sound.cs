﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Sound 
{
    
        public string Name;
        public AudioClip SoundClip;

        [Range(0f, 1f)]
        public float Volume;

        [Range(0f, 1f)]
        public float Pitch;

        public bool Repeat;

        [HideInInspector]
        public AudioSource audioSource;
    
}
