﻿using System;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public Sound[] sounds;
    public static SoundManager instance;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);

        foreach (Sound s in sounds)
        {
            s.audioSource = gameObject.AddComponent<AudioSource>();
            s.audioSource.clip = s.SoundClip;
            s.audioSource.volume = s.Volume;
            s.audioSource.pitch = s.Pitch;
            s.audioSource.loop = s.Repeat;
        }
    }
    public void PlaySound(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.Name == name);
        if (s == null)
        {
            
            Debug.LogWarning("Sonido: " + name + " no encontrado.");
            return;
        }
        else
        {
            s.audioSource.Play();           
        }
    }

    public void PauseSound(string name)
    {
        Sound s = Array.Find(sounds, sonido => sonido.Name == name);
        if (s == null)
        {
            Debug.LogWarning("Sonido: " + name + " no encontrado.");
            return;
        }
        else
        {
            s.audioSource.Pause();
        }
    }
    public void UnpauseSound(string name)
    {
        Sound s = Array.Find(sounds, sonido => sonido.Name == name);
        if (s == null)
        {
            Debug.LogWarning("Sonido: " + name + " no encontrado.");
            return;
        }
        else
        {
            s.audioSource.UnPause();
        }
    }
    public void StopSound(string name)
    {
        Sound s = Array.Find(sounds, sonido => sonido.Name == name);
        if (s == null)
        {
            Debug.LogWarning("Sonido: " + name + " no encontrado.");
            return;
        }
        else
        {
            s.audioSource.Stop();
        }
    }
}
