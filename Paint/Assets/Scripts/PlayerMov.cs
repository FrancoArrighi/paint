using UnityEngine;

public class PlayerMov : MonoBehaviour
{
    public float vel;
    Rigidbody rb;
    public GameObject interactionImg;
    public bool nearInteraction;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        interactionImg.SetActive(false);
    }
    void Update()
    {
        HandleMovement();
    }
    public void HandleMovement()
    {
        float movVertical = Input.GetAxisRaw("Vertical");
        float movHorizontal = Input.GetAxisRaw("Horizontal");

        Vector3 velocity = Vector3.zero;
        if (movVertical != 0 || movHorizontal != 0)
        {
            Vector3 dir = (transform.forward * movVertical + transform.right * movHorizontal).normalized;
            velocity = dir * vel;
        }
        rb.velocity = velocity;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("interactable") && !GameManager.instance.playerPainting)
        {
            nearInteraction = true;
            interactionImg.SetActive(true);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("interactable") && !GameManager.instance.playerPainting)
        {
            interactionImg.SetActive(false);
            nearInteraction = false;
        }
    }
}
