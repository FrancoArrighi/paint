using Unity.Burst.Intrinsics;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    Camera _camera;
    Animator _camAnimator;

    [SerializeField] private Animator _fadeAnimator;
    [SerializeField] private GameObject _txtStart;
    [SerializeField] private GameObject _texts;
    bool gameStarted = false;

    public static GameManager instance;

    public PlayerMov playerMov;
    public CameraMov cameraMov;

    public bool playerPainting = false;
    private bool finishedAnimation = false;
    public bool startedPainting = false;
    public bool savingPicture = false;
    public CamMovements camMovements;
    public BoxCollider colliderPaint;
    public CapsuleCollider colliderPlayer;
    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        _camera = Camera.main;
        _camAnimator = _camera.GetComponent<Animator>();
        colliderPlayer.enabled = false;
    }

    private void PlayMusic()
    {
        SoundManager.instance.PlaySound("Music");
    }
    private void PlayCamAnim()
    {
        _camAnimator.Play("AnimCamera");
    }
    private void FinishedAnimation()
    {
        _texts.SetActive(true);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        finishedAnimation = true;
        playerPainting = true;
        colliderPaint.enabled = false;
        _camAnimator.enabled = false;
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && !gameStarted)
        {
            _txtStart.SetActive(false);
            gameStarted = true;
            _fadeAnimator.Play("Fade");
            Invoke("PlayMusic", 16f);
            Invoke("PlayCamAnim", 3f);
            Invoke("FinishedAnimation", 23f);
        }

        if (!finishedAnimation) { return; }

        if (playerPainting && Input.GetKeyDown(KeyCode.Escape))
        {
            if (!savingPicture)
            {
                StopPainting();
            }
            else
            {
                SaveSystem.instance.SaveInterface.SetActive(false);
                savingPicture = false;
            }
        }
        if (playerMov.nearInteraction && Input.GetMouseButtonDown(0))
        {
            StartPainting();
        }
    }
    public void StartPainting()
    {
        playerPainting = true;
        cameraMov.enabled = false;
        playerMov.enabled = false;
        playerMov.nearInteraction = false;
        startedPainting = true;
        camMovements.enabled = true;
        cameraMov.transform.parent = null;
        _texts.SetActive(true);
        playerMov.interactionImg.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        colliderPaint.enabled = false;
        colliderPlayer.enabled = false;
    }
    public void StopPainting()
    {
        cameraMov.enabled = true;
        playerMov.enabled = true;
        playerPainting = false;
        playerMov.nearInteraction = false;
        camMovements.enabled = false;
        cameraMov.transform.parent = playerMov.transform;
        cameraMov.transform.localPosition = new Vector3(0, 0.5f, 0);
        cameraMov.transform.Rotate(Vector3.zero);
        colliderPaint.enabled = true;
        _texts.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        colliderPaint.enabled = true;
        colliderPlayer.enabled = true;
    }

}
