using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GalleryManager : MonoBehaviour
{
    public PictureInGallery[] picturesInGallery;
    public int countPictures = 0;
    public Material[] mats_frame;
    public static GalleryManager instance;

    private void Awake()
    {
        instance = this;
    }

    public Sprite GenerateTexture(Texture2D texture)
    {
        Sprite newSprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));

        countPictures++;
        return newSprite;
    }
    public void SetPictureInGallery(Texture2D texture, string txt_plaque, int posGallery, int mat_frameIndex)
    {
        Sprite sprite = GenerateTexture(texture);
        picturesInGallery[posGallery - 1].SetPictureInGallery(sprite, txt_plaque, mats_frame[mat_frameIndex - 1]);
    }

    public void SetInitialPictures(List<byte[]> screenshots)
    {
        PicturesInfoSerializer plaquesSystem = FindObjectOfType<PicturesInfoSerializer>();
        plaquesSystem.LoadPicturesInfoFromFile();

        foreach (PictureInfo pictureInfo in plaquesSystem.pictures)
        {
            if (pictureInfo.inGallery) // siempre va a haber maximo 10 pictureInfo con inGallery = true
            {
                Texture2D texture = new Texture2D(1000, 1000);
                texture.LoadImage(screenshots[pictureInfo.posInFolder - 1]);

                SetPictureInGallery(texture, pictureInfo.txt_plaque, pictureInfo.posInGallery, pictureInfo.frameType);
            }
        }
    }
}
