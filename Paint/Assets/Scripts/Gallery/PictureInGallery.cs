using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PictureInGallery : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;
    public TMP_Text txt_plaque;
    public MeshRenderer frame;

    public void SetPictureInGallery(Sprite sprite, string _txt_plaque, Material mat_frame)
    {
        txt_plaque.text = _txt_plaque;
        spriteRenderer.sprite = sprite;
        frame.sharedMaterial = mat_frame;
        ResizeSpriteToFit();
    }
    void ResizeSpriteToFit()
    {
        if (spriteRenderer.sprite == null) return;

        Bounds spriteBounds = spriteRenderer.sprite.bounds;
        Vector3 parentSize = spriteRenderer.transform.parent.GetComponent<Renderer>().bounds.size;

        Vector3 newScale = spriteRenderer.transform.localScale;
        newScale.x = parentSize.x / spriteBounds.size.x;
        newScale.y = parentSize.y / spriteBounds.size.y;
        spriteRenderer.transform.localScale = newScale;
    }
}